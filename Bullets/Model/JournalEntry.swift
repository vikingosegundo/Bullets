//
//  JournalEntry.swift
//  Bullets
//
//  Created by Manuel Meyer on 02.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation

class JournalEntry: NSObject, JournalEntryModel, NSCoding {
    
    convenience init(title: String? = nil) {
        self.init(title:title, uuid: NSUUID().uuidString)
    }
    
    required convenience init?(coder decoder: NSCoder) {
        guard let title = decoder.decodeObject(forKey: "title") as? String,
            let uuid = decoder.decodeObject(forKey: "uuid") as? String
        else {
            return nil
        }
        self.init(title:title, uuid: uuid)
    }
    
    private init(title: String?, uuid: String) {
        self.title = title
        self.uuid = uuid
    }
    
    let uuid: String
    var title:String?
    
    func encode(with coder: NSCoder) {
        coder.encode(self.title, forKey: "title")
        coder.encode(self.uuid,  forKey: "uuid")
    }
    
    func entryUUID() -> String {
        return uuid
    }
}
