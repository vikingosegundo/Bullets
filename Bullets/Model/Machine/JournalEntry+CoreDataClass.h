//
//  JournalEntry+CoreDataClass.h
//  
//
//  Created by Manuel Meyer on 18.07.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface JournalEntry : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "JournalEntry+CoreDataProperties.h"
