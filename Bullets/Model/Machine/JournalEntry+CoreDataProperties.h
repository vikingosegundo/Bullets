//
//  JournalEntry+CoreDataProperties.h
//  
//
//  Created by Manuel Meyer on 18.07.16.
//
//

#import "JournalEntry+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface JournalEntry (CoreDataProperties)

+ (NSFetchRequest<JournalEntry *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *uuid;

@end

NS_ASSUME_NONNULL_END
