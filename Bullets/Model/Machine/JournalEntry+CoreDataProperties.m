//
//  JournalEntry+CoreDataProperties.m
//  
//
//  Created by Manuel Meyer on 18.07.16.
//
//

#import "JournalEntry+CoreDataProperties.h"

@implementation JournalEntry (CoreDataProperties)

+ (NSFetchRequest<JournalEntry *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"JournalEntry"];
}

@dynamic title;
@dynamic uuid;

@end
