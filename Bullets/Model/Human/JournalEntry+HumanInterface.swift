//
//  JournalEntry+HumanInterface.swift
//  Bullets
//
//  Created by Manuel Meyer on 29.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

extension JournalEntry: JournalEntryModel {
    func entryUUID() -> String {
        return self.uuid!
    }
}
