//
//  JournalEntryModel.swift
//  Bullets
//
//  Created by Manuel Meyer on 02.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation

protocol JournalEntryModel : class, AnyObject {
    var title:String? {get set}
    func entryUUID() -> String
}
