//
//  File.swift
//  Bullets
//
//  Created by Manuel Meyer on 30.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

protocol JournalEntryFetcherAccessor: class {
    var journalEntryFetcher: JournalEntryFetching! {set get}
}


protocol JournalEntryCreatorAccessor: class {
    var journalEntryCreator: JournalEntryCreating! {set get}
}

protocol JournalEntryEditorAccessor: class {
    var journalEntryEditor: JournalEntryEditing! {set get}
}


protocol JournalEntryRemoverAccessor: class {
    var journalEntryRemover: JournalEntryRemoving! {set get}
}


