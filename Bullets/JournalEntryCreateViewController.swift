//
//  JournalEntryCreateViewController.swift
//  Bullets
//
//  Created by Manuel Meyer on 01.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import UIKit

class JournalEntryCreateViewController: JournalEntryBaseViewController,
                                        JournalEntryCreatorAccessor,
                                        JournalEntryEditorAccessor
{
    @IBOutlet weak var titleTextField: UITextField! {
        didSet{
            titleTextField.addTarget(self, action: #selector(JournalEntryCreateViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        }
    }
    var journalEntryCreator: JournalEntryCreating!
    var journalEntryEditor: JournalEntryEditing!

    func textFieldDidChange(_ sender: UITextField) {
        if let title = sender.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                 where title.characters.count > 0 {
            if self.journalEntry == nil {
                _ = try? self.journalEntry = journalEntryCreator.createJournalEntry(title)
            } else {
                self.journalEntryEditor.changeTitle(title, journalEntry: journalEntry)
            }
        }
    }
}
