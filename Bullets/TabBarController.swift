//
//  TabBarController.swift
//  Bullets
//
//  Created by Manuel Meyer on 29.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController,
                        JournalEntryFetcherAccessor,
                        JournalEntryCreatorAccessor,
                        JournalEntryEditorAccessor,
                        JournalEntryRemoverAccessor
{
    var journalEntryCreator: JournalEntryCreating!{
        didSet{
            configureSelectedViewController()
        }
    }
    
    var journalEntryFetcher: JournalEntryFetching!{
        didSet{
            configureSelectedViewController()
        }
    }

    var journalEntryEditor: JournalEntryEditing!{
        didSet{
            configureSelectedViewController()
        }
    }

    var journalEntryRemover: JournalEntryRemoving!{
        didSet{
            configureSelectedViewController()
        }
    }
    
    override var selectedViewController: UIViewController?{
        didSet{
            configureSelectedViewController()
        }
    }
    
    private func configureSelectedViewController(){
        if let journalEntryFetcherAccessor = self.selectedViewController as? JournalEntryFetcherAccessor,
            journalEntryFetcher = self.journalEntryFetcher where journalEntryFetcherAccessor.journalEntryFetcher == nil {
            journalEntryFetcherAccessor.journalEntryFetcher = journalEntryFetcher
        }
        
        if let journalEntryCreatorAccessor = self.selectedViewController as? JournalEntryCreatorAccessor,
            journalEntryCreator = self.journalEntryCreator where journalEntryCreatorAccessor.journalEntryCreator == nil {
            journalEntryCreatorAccessor.journalEntryCreator = journalEntryCreator
        }
        
        if let journalEntryEditorAccessor = self.selectedViewController as? JournalEntryEditorAccessor,
            journalEntryEditor = self.journalEntryEditor where journalEntryEditorAccessor.journalEntryEditor == nil{
            journalEntryEditorAccessor.journalEntryEditor = journalEntryEditor
        }
        
        if let journalEntryRemoverAccessor = self.selectedViewController as? JournalEntryRemoverAccessor,
            journalEntryRemover = self.journalEntryRemover where journalEntryRemoverAccessor.journalEntryRemover == nil{
            journalEntryRemoverAccessor.journalEntryRemover = journalEntryRemover
        }
    }
}
