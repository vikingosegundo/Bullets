//
//  JournalViewController
//  Bullets
//
//  Created by Manuel Meyer on 29.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import UIKit

class JournalViewController: UIViewController,
                                JournalEntryFetcherAccessor,
                                JournalEntryCreatorAccessor,
                                JournalEntryEditorAccessor,
                                JournalEntryRemoverAccessor
{
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            configureDataSource()
        }
    }

    var journalEntryFetcher: JournalEntryFetching! {
        didSet{
            configureDataSource()
        }
    }
    
    var journalEntryCreator     : JournalEntryCreating!
    var journalEntryEditor      : JournalEntryEditing!
    var journalEntryRemover     : JournalEntryRemoving!
    var dataSource              : JournalViewControllerDataSource!
    
    private var selectedJournalEntry: JournalEntryModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.selectedJournalEntry = nil
        self.tableView.reloadData()
    }
    
    @IBAction func addJournalEnrtry(_ sender: AnyObject) {
        performSegue(withIdentifier: "CreateJournalEntry", sender: nil)
    }
    
    @IBAction func unwindToTodayViewcontroller(_ segue: UIStoryboardSegue) {
    }
    
    func editJournalEntry(_ journalEntry: JournalEntryModel) {
        self.selectedJournalEntry = journalEntry
        self.performSegue(withIdentifier: "EditJournalEntry", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        if let dvc = segue.destinationViewController as? JournalEntryBaseViewController, selectedJournalEntry = self.selectedJournalEntry { dvc.journalEntry          = selectedJournalEntry }
        if let dvc = segue.destinationViewController as? JournalEntryCreatorAccessor,    journalEntryCreator  = self.journalEntryCreator  { dvc.journalEntryCreator   = journalEntryCreator  }
        if let dvc = segue.destinationViewController as? JournalEntryEditorAccessor,     journalEntryEditor   = self.journalEntryEditor   { dvc.journalEntryEditor    = journalEntryEditor   }
        if let dvc = segue.destinationViewController as? JournalEntryRemoverAccessor,    journalEntryRemover  = self.journalEntryRemover  { dvc.journalEntryRemover   = journalEntryRemover  }
    }
    
    private func configureDataSource(){
        if let todayJournalEntryFetcher = self.journalEntryFetcher, tableView = self.tableView {
            do {
                self.dataSource = try JournalViewControllerDataSource(view: tableView, todayJournalEntryFetcher: todayJournalEntryFetcher)
                self.dataSource.journalEntrySeletced = {
                    entry in
                    self.editJournalEntry(entry)
                }
            } catch JournalViewControllerDataSourceError.neitherCollectionNorTableView {
                assertionFailure("I need a datasource.")
            } catch  {
                NSLog("Something else went wrong.")
            }
        }
    }
}

