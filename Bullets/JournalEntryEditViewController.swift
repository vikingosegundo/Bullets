//
//  JournalEntryEditViewController.swift
//  Bullets
//
//  Created by Manuel Meyer on 30.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import UIKit

class JournalEntryEditViewController: JournalEntryBaseViewController,
                                        JournalEntryEditorAccessor,
                                        JournalEntryRemoverAccessor
{
    
    @IBOutlet weak var titleTextField: UITextField!
    
    var journalEntryEditor    : JournalEntryEditing!
    var journalEntryRemover   : JournalEntryRemoving!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let title = self.journalEntry.title {
            self.titleTextField.text = title
        }
    }
    
    @IBAction func removeAction(_ sender: AnyObject) {
        if let journalEntry = self.journalEntry {
            let alert = UIAlertController(title: "Delete?", message: "Do you want to delete this journal entry permanently?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {
                action in
                self.journalEntryRemover.deleteJournalEntry(journalEntry)
                self.journalEntry = nil
                self.performSegue(withIdentifier: "UnwindToTodayViewcontroller", sender: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let journalEntry = self.journalEntry {
            if let title = self.titleTextField.text?.trimmingCharacters(
            in: CharacterSet.whitespacesAndNewlines
                ) where title.characters.count > 0 {
                self.journalEntryEditor.changeTitle(title, journalEntry: journalEntry)
            }
        }
    }
}
