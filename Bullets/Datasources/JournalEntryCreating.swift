//
//  JournalEntryCreating.swift
//  Bullets
//
//  Created by Manuel Meyer on 18.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//


protocol JournalEntryCreating {
    func createJournalEntry(_ title:String?) throws -> JournalEntryModel
}
