//
//  TodayViewControllerDatasource.swift
//  Bullets
//
//  Created by Manuel Meyer on 29.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import UIKit
import OFAPopulator


enum JournalViewControllerDataSourceError : ErrorProtocol {
    case neitherCollectionNorTableView
}

class JournalViewControllerDataSource {
    
    init(view:UIView, todayJournalEntryFetcher: JournalEntryFetching) throws{
        self.todayJournalEntryFetcher = todayJournalEntryFetcher
        
        if let collectionView = view as? UICollectionView {
            self.collectionView = collectionView
        } else if let tableView = view as? UITableView {
            self.tableView = tableView
        } else {
            throw JournalViewControllerDataSourceError.neitherCollectionNorTableView
        }
        setupPopulator()
    }
    
    var journalEntrySeletced: ((JournalEntryModel) -> ())?
    
    var view: UIView! {
        if let collectionView = self.collectionView {
            return collectionView
        }
        if let tableView = self.tableView {
            return tableView
        }
        assertionFailure("view must be an UITableView or an UICollectionView")
        return nil
    }
    
    private var collectionView: UICollectionView?
    private var tableView:UITableView?
    
    let todayJournalEntryFetcher: JournalEntryFetching
    var populator: OFAViewPopulator!
    
    private func setupPopulator(){
        let section1Populator = OFASectionPopulator(parentView: self.view, dataProvider: JournalEntriesDataProvider(journalEntryFetcher: self.todayJournalEntryFetcher), cellIdentifier: { (obj, indexPath) -> String! in
                return "Cell"
            }) { (obj, cell, indexPath) in
                if let cell = cell as? UITableViewCell, obj = obj as? JournalEntryModel{
                    cell.textLabel?.text = obj.title
                }
        }!
        section1Populator.objectOnCellSelected = {
            obj, view, indexPath in
            if let entry = obj as? JournalEntryModel {
                self.journalEntrySeletced?(entry)
            }
        }
        self.populator = OFAViewPopulator(sectionPopulators: [section1Populator])
    }
}
