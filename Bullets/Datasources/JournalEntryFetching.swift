//
//  JournalEntryFetching.swift
//  Bullets
//
//  Created by Manuel Meyer on 30.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

protocol JournalEntryFetching {
    func allEntries() -> [AnyObject]
}
