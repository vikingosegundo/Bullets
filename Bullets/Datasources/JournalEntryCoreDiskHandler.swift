//
//  JournalEntryCoreDiskHandler.swift
//  Bullets
//
//  Created by Manuel Meyer on 02.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation

class JournalEntryDiskHandler {
    init(journalDirectoryPath:String){
        self.journalDirectoryPath = journalDirectoryPath
    }
    
    let journalDirectoryPath:String
    
    func saveJournalEntry(journalEntry: JournalEntryModel) {
        if journalEntry is NSCoding {
            let uuid = journalEntry.entryUUID()
            let entryPath = NSURL(fileURLWithPath: journalDirectoryPath).appendingPathComponent(uuid)!.path!
            NSKeyedArchiver.archiveRootObject(journalEntry, toFile: entryPath)
        }
    }
}
