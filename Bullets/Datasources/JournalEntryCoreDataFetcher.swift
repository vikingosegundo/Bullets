//
//  JournalEntryCoreDataFetcher.swift
//  Bullets
//
//  Created by Manuel Meyer on 02.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation

class JournalEntryCoreDataFetcher: JournalEntryFetching {
    
    init(managedObjectContext:NSManagedObjectContext){
        self.managedObjectContext = managedObjectContext
    }
    
    let managedObjectContext:NSManagedObjectContext
    
    func allEntries() -> [AnyObject] {
        let fetchRequest: NSFetchRequest<JournalEntry> = NSFetchRequest(entityName: "JournalEntry")
        fetchRequest.sortDescriptors = [SortDescriptor(key: "title", ascending: true, selector:#selector(NSString.caseInsensitiveCompare(_:)) )]
        return try! managedObjectContext.fetch(fetchRequest)
    }
}
