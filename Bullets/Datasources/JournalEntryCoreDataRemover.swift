//
//  JournalEntryCoreDataRemover.swift
//  Bullets
//
//  Created by Manuel Meyer on 18.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation

class JournalEntryCoreDataRemover: JournalEntryRemoving {
    
    init(managedObjectContext:NSManagedObjectContext){
        self.managedObjectContext = managedObjectContext
    }
    
    let managedObjectContext:NSManagedObjectContext
    
    func deleteJournalEntry(_ journalEntry: JournalEntryModel) {
        if let journalEntry = journalEntry as? JournalEntry {
            managedObjectContext.delete(journalEntry)
            _ = try? managedObjectContext.save()
            
        }
    }
    
}
