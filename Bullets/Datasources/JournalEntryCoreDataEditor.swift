//
//  JournalEntryDeitingCoreData.swift
//  Bullets
//
//  Created by Manuel Meyer on 02.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation

class JournalEntryCoreDataEditor: JournalEntryEditing {
    
    init(managedObjectContext:NSManagedObjectContext){
        self.managedObjectContext = managedObjectContext
    }
    
    let managedObjectContext:NSManagedObjectContext
    
    func changeTitle(_ title: String?, journalEntry: JournalEntryModel) {
        if let journalEntry = journalEntry as? JournalEntry {
            journalEntry.title = title
            _ = try? managedObjectContext.save()
        }
    }
}
