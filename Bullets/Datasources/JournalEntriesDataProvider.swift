//
//  JournalEntriesDataProvider.swift
//  Bullets
//
//  Created by Manuel Meyer on 30.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import OFAPopulator

class JournalEntriesDataProvider: NSObject, OFASectionDataProvider {
    let journalEntryFetcher: JournalEntryFetching
    
    init(journalEntryFetcher: JournalEntryFetching) {
        self.journalEntryFetcher = journalEntryFetcher
    }
    
    var sectionObjects: [AnyObject]! {
        set{}
        get{
            return self.journalEntryFetcher.allEntries()
        }
    }
}
