//
//  JournalEntryeEditingDisk.swift
//  Bullets
//
//  Created by Manuel Meyer on 02.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation



class JournalEntryDiskCreator:JournalEntryDiskHandler, JournalEntryCreating {
    func createJournalEntry(_ title: String?) throws  -> JournalEntryModel {
        let entry = JournalEntry(title: title)
        saveJournalEntry(journalEntry: entry)
        return entry
    }
}


class JournalEntryDiskEditor:JournalEntryDiskHandler, JournalEntryEditing {
    
    func changeTitle(_ title: String?, journalEntry: JournalEntryModel) {
        if let journalEntry = journalEntry as? JournalEntry {
            journalEntry.title = title
            saveJournalEntry(journalEntry: journalEntry)
        }
    }
}

class JournalEntryDiskRemover:JournalEntryDiskHandler, JournalEntryRemoving {
    
    func deleteJournalEntry(_ journalEntry: JournalEntryModel) {
        do{
            try FileManager.default().removeItem(atPath: try URL(fileURLWithPath: self.journalDirectoryPath).appendingPathComponent(journalEntry.entryUUID()).path!)
        }
        catch {
            
        }
    }
    
}
