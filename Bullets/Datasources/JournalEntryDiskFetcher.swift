//
//  JournalEntryDiskFetcher.swift
//  Bullets
//
//  Created by Manuel Meyer on 02.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation

class JournalEntryDiskFetcher:JournalEntryDiskHandler, JournalEntryFetching {

    func allEntries() -> [AnyObject] {
        let dictionaryList = dictionaryListing()
        
        let entries = dictionaryList.filter { (url) -> Bool in
            if let _ = NSKeyedUnarchiver.unarchiveObject(withFile: url.path!) as? JournalEntry
            {
                return true
            }
            return false
        }.map { (url) -> JournalEntry in
            return NSKeyedUnarchiver.unarchiveObject(withFile: url.path!) as! JournalEntry
        }
        return entries.sorted(isOrderedBefore: { (e1, e2) -> Bool in
            guard let t1 = e1.title, t2 = e2.title else { return false }
            return t1.localizedCaseInsensitiveCompare(t2) == .orderedAscending
        })
    }
    
    private func dictionaryListing() -> [URL]{
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default().contentsOfDirectory(at:URL(fileURLWithPath: self.journalDirectoryPath), includingPropertiesForKeys: nil, options: [])
            
            return directoryContents
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return []
    }
}
