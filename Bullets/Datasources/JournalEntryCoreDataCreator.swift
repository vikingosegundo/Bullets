//
//  JournalEntryCoreDataCreator.swift
//  Bullets
//
//  Created by Manuel Meyer on 18.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import Foundation


class JournalEntryCoreDataCreator: JournalEntryCreating {
    init(managedObjectContext:NSManagedObjectContext){
        self.managedObjectContext = managedObjectContext
    }
    
    let managedObjectContext:NSManagedObjectContext
    
    func createJournalEntry(_ title: String?) throws  -> JournalEntryModel {
        let entry = NSEntityDescription.insertNewObject(forEntityName: "JournalEntry",
                                                        into: managedObjectContext) as! JournalEntry
        entry.title = title
        try managedObjectContext.save()        
        return entry
    }
}
