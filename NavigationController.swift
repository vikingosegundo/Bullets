//
//  NavigationController.swift
//  Bullets
//
//  Created by Manuel Meyer on 30.06.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController,
                            JournalEntryFetcherAccessor,
                            JournalEntryCreatorAccessor,
                            JournalEntryEditorAccessor,
                            JournalEntryRemoverAccessor
{
    var journalEntryFetcher     : JournalEntryFetching!
    var journalEntryCreator     : JournalEntryCreating!
    var journalEntryEditor      : JournalEntryEditing!
    var journalEntryRemover     : JournalEntryRemoving!
    
    override var topViewController: UIViewController?{
        get{
            let vc = super.topViewController

            if let vc = vc as? JournalEntryFetcherAccessor,
                journalEntryFetcher = self.journalEntryFetcher where vc.journalEntryFetcher == nil {
                vc.journalEntryFetcher = journalEntryFetcher
            }
            
            if let vc = vc as? JournalEntryCreatorAccessor,
                journalEntryCreator = self.journalEntryCreator where vc.journalEntryCreator == nil {
                vc.journalEntryCreator = journalEntryCreator
            }
            
            if let journalEntryCreatorAccessor = vc as? JournalEntryCreatorAccessor,
                journalEntryCreator = self.journalEntryCreator where journalEntryCreatorAccessor.journalEntryCreator == nil {
                journalEntryCreatorAccessor.journalEntryCreator = journalEntryCreator
            }
            
            if let journalEntryEditorAccessor = vc as? JournalEntryEditorAccessor,
                journalEntryEditor = self.journalEntryEditor where journalEntryEditorAccessor.journalEntryEditor == nil {
                journalEntryEditorAccessor.journalEntryEditor = journalEntryEditor
            }
            
            if let journalEntryDeleteterAccessor = vc as? JournalEntryRemoverAccessor,
                journalEntryRemover = self.journalEntryRemover where journalEntryDeleteterAccessor.journalEntryRemover == nil {
                journalEntryDeleteterAccessor.journalEntryRemover = journalEntryRemover
            }
            return vc
        }
    }
}
