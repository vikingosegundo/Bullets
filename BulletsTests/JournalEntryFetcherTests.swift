//
//  JournalEntryFetcherTests.swift
//  Bullets
//
//  Created by Manuel Meyer on 07.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import XCTest
import CoreData
@testable import Bullets

class JournalEntryFetcherTests: XCTestCase {
    
    
    var sut: JournalEntryCoreDataFetcher!
    var managedObjectContext: NSManagedObjectContext!
    
    override func setUp() {
        super.setUp()
        managedObjectContext = setUpInMemoryManagedObjectContext()
        sut = JournalEntryCoreDataFetcher(managedObjectContext:managedObjectContext)
        
        let e1 = NSEntityDescription.insertNewObject(forEntityName: "JournalEntry",
                                                    into: managedObjectContext) as! JournalEntry
        let e2 = NSEntityDescription.insertNewObject(forEntityName: "JournalEntry",
                                                    into: managedObjectContext) as! JournalEntry
        e1.title = "Entry 1"
        e2.title = "Entry 2"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFetchingAllEntries() {
        XCTAssert(sut.allEntries().count == 2)
    }
    
    func testFetchCorrectEntry()  {
        XCTAssert((sut.allEntries()[0] as! JournalEntry).title! == "Entry 1")
        XCTAssert((sut.allEntries()[1] as! JournalEntry).title! == "Entry 2")
    }
    
}
