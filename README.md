# A bullet-proof SOLID implementation

This project illustrates how applying [SOLID principles](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design)) during development leads to 
less coupling, higher re-usability and testability. 

For example: The target "Bullets" uses Core Data, the tartget "BulletsDisk" 
writes everything directly to the disk.  

As all dependencies are injected during construction or via properties (["Dependency 
Inversion Principle"](https://en.wikipedia.org/wiki/Dependency_inversion_principle)), the interfaces follow the ["Interface Segregation Principle"](https://en.wikipedia.org/wiki/Interface_segregation_principle) 
and exposed paramters are typed as protocols (["Liskov Substitution Principle"](https://en.wikipedia.org/wiki/Liskov_substitution_principle)),
which implementations are doing exactly one thing ["Single Responsibility Principle"](https://en.wikipedia.org/wiki/Single_responsibility_principle), exchanging one persitancy layer
against the other is rather simple and dont require any code changes in views, 
viewcontrollers and data sources.

This project does *not* illustrate best practices for core data or performant 
disk operations. i.e. caching and concurent operations aren't used. 

It uses [OFAPopulator](http://vikingosegundo.github.io/ofapopulator/) — my framework to 
populate tableviews and collectionviews easily in a SOLID fashion without forcing 
the developer into certain contraints as ie using certain subclasses.

This project is written in Swift 3. Please open it in Xcode 8 and higher. 