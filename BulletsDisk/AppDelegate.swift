//
//  AppDelegate.swift
//  BulletsDisk
//
//  Created by Manuel Meyer on 02.07.16.
//  Copyright © 2016 Manuel Meyer. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let path = self.pathForJournal()
        
        if let journalEntryFetcherAccessor = window!.rootViewController as? JournalEntryFetcherAccessor where journalEntryFetcherAccessor.journalEntryFetcher == nil {
            journalEntryFetcherAccessor.journalEntryFetcher = JournalEntryDiskFetcher(journalDirectoryPath: path)
        }
        
        if let journalEntryCreatorAccessor = window!.rootViewController as? JournalEntryCreatorAccessor where journalEntryCreatorAccessor.journalEntryCreator == nil {
            journalEntryCreatorAccessor.journalEntryCreator = JournalEntryDiskCreator(journalDirectoryPath: path)
        }
        
        if let journalEntryEditorAccessor = window!.rootViewController as? JournalEntryEditorAccessor where journalEntryEditorAccessor.journalEntryEditor == nil {
            journalEntryEditorAccessor.journalEntryEditor = JournalEntryDiskEditor(journalDirectoryPath: path)
        }
        
        if let journalEntryRemoverAccessor = window!.rootViewController as? JournalEntryRemoverAccessor where journalEntryRemoverAccessor.journalEntryRemover == nil{
            journalEntryRemoverAccessor.journalEntryRemover = JournalEntryDiskRemover(journalDirectoryPath: path)
        }
        return true
    }

    private func pathForJournal() -> String {
        let fileManager = FileManager.default()
        let urls = fileManager.urlsForDirectory(.documentDirectory, inDomains: .userDomainMask)
        
        guard let path = urls.first else { return "" }

        let journalDictionaryPath = try! path.appendingPathComponent("Journal")
        createDictionaryIfNecessary(fullPath: journalDictionaryPath.path!)
        return journalDictionaryPath.path!
    }
    
    private func createDictionaryIfNecessary(fullPath: String) {
        let fileManager = FileManager.default()
        var isDir : ObjCBool = false
        if !fileManager.fileExists(atPath: fullPath, isDirectory:&isDir) {
            _ = try! fileManager.createDirectory(at: URL(fileURLWithPath: fullPath), withIntermediateDirectories: true, attributes: [:])
        }
    }

}

